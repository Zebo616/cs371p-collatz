// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, test_eval_big) {
    ASSERT_EQ(collatz_eval(make_pair(9, 99999)), make_tuple(9, 99999, 351));
}

TEST(CollatzFixture, test_eval_failed) {
    ASSERT_EQ(collatz_eval(make_pair(883072, 879660)), make_tuple(883072, 879660, 344));
}

TEST(CollatzFixture, test_eval_failed_2) {
    ASSERT_EQ(collatz_eval(make_pair(151000, 150000)),make_tuple(151000, 150000, 370));
}

TEST(CollatzFixture, test_eval_failed_3) {
    ASSERT_EQ(collatz_eval(make_pair(691999, 703414)), make_tuple(691999, 703414, 411));
}

TEST(CollatzFixture, test_eval_failed_4) {
    ASSERT_EQ(collatz_eval(make_pair(152000, 151000)),make_tuple(152000, 151000, 295));
}

TEST(CollatzFixture, test_eval_small) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2)),make_tuple(1, 2, 2));
}

TEST(CollatzFixture, test_eval_sameNum) {
    ASSERT_EQ(collatz_eval(make_pair(100, 100)),make_tuple(100, 100, 26));
}

TEST(CollatzFixture, test_eval_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)),make_tuple(10, 1, 20));
}

TEST(CollatzFixture, test_eval_lowerEdge) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)),make_tuple(1, 1, 1));
}

TEST(CollatzFixture, test_eval_higherEdge) {
    ASSERT_EQ(collatz_eval(make_pair(999998, 999999)),make_tuple(999998, 999999, 259));
}

TEST(CollatzFixture, eval0_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval1_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(200, 100)), make_tuple(200, 100, 125));
}

TEST(CollatzFixture, eval2_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(210, 201)), make_tuple(210, 201, 89));
}

TEST(CollatzFixture, eval3_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(1000, 900)), make_tuple(1000, 900, 174));
}

TEST(CollatzFixture, test_eval_big_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(99999, 9)), make_tuple(99999, 9, 351));
}

TEST(CollatzFixture, test_eval_failed_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(879660, 883072)), make_tuple(879660, 883072, 344));
}

TEST(CollatzFixture, test_eval_failed_3_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(703414, 691999)), make_tuple(703414, 691999, 411));
}

TEST(CollatzFixture, test_eval_small_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(2, 1)),make_tuple(2, 1, 2));
}

TEST(CollatzFixture, test_eval_higherEdge_reverse) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 999998)),make_tuple(999999, 999998, 259));
}

TEST(CollatzFixture, test_eval_find) {
    ASSERT_EQ(find(1), 1);
}

TEST(CollatzFixture, test_eval_find2) {
    ASSERT_EQ(find(10), 7);
}

// This is the test template.
// TEST(CollatzFixture, test_eval_higherEdge) {
//     ASSERT_EQ(collatz_eval(make_pair(, )),make_tuple(, , ));
// }

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    istringstream coveragesin("");
    ostringstream coveragesout;
    collatz_solve(coveragesin, coveragesout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
