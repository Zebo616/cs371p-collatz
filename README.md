# CS371p: Object-Oriented Programming Collatz Repo

* Name: Steven Callahan

* EID: svc434

* GitLab ID: Zebo616

* HackerRank ID: stevio616

* Git SHA: 577b57ef07f6b52cd800d0877166da7d6f196f21

* GitLab Pipelines: #253978993

* Estimated completion time: 4 hours

* Actual completion time: 5 hours

* Comments: Much of the code used in this project was adapted from my CS373-Collatz repository, which can be found here:
https://gitlab.com/Zebo616/cs373-collatz
This code was adapted with permission from Downing.
